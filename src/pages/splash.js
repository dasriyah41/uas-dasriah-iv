import React, {useEffect} from 'react';
import {Image, StyleSheet, Text,View} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout (() => {
            navigation.replace('Welcome');
        }, 5000);
    });
    return(
        <View style={styles.wrapper}>
            <Text style={styles.welcomeText}>DOA-DOA ANAK</Text>
            <Image source={require('../assets/animasi.png')} style={{width:400,height:400}}/>
        </View>
    );
};

export default Splash;

const styles =StyleSheet.create({
    wrapper: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        
    },
    welcomeText: {
        fontSize: 30,
        fontWeight: 'bold',
        color: 'green',
        paddingBottom: 15,
    },
});
