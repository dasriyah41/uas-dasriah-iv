import React, { Component } from 'react';
import {Image, StyleSheet,Text, View, ScrollView, TouchableOpacity,FlatList, Dimensions} from 'react-native';

const numColumns = 2;
const WIDTH = Dimensions.get('window').width;

class WelcomeAuth extends Component {
    constructor(props) {
        super(props);
        this.state ={
            image : [
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Sebelum Makan')}><Image source={require('../assets/13.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:1},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Sesudah Makan')}><Image source={require('../assets/1.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:2},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Sebelum Tidur')}><Image source={require('../assets/2.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:3},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Bangun Tidur')}><Image source={require('../assets/3.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:4},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Masuk Masjid')}><Image source={require('../assets/4.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:5},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Keluar Masjid')}><Image source={require('../assets/5.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:6},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Masuk Rumah')}><Image source={require('../assets/7.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:7},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Keluar Rumah')}><Image source={require('../assets/8.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:8},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Sebelum Belajar')}><Image source={require('../assets/11.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:9},
                {doa:<TouchableOpacity onPress={() => this.props.navigation.navigate('Doa Sesudah Belajar')}><Image source={require('../assets/10.jpg')} style={{width:200,height:200}}/></TouchableOpacity>,nomor:10},
                
            ],
        };
    }
    render(){
    return(
        <View style={Styles.wrapper}>
            <FlatList
                data = {this.state.image}
                renderItem = {({item,index}) =>(
                <View>{item.doa}</View>
                )}
                keyExtractor = {(item) => item.nomor}
                numColumns = {numColumns}
            />
        </View>
    );
    }
};

export default WelcomeAuth;

const Styles =StyleSheet.create({
    wrapper: {
        backgroundColor: 'black',
        justifyContent:'center',
        alignContent:'center',
        flex:1,
    },
    ukuran : {
      height : WIDTH / (numColumns * 2),
      margin: 5,
      padding : 5,
      borderRadius : 5,
      justifyContent : 'center',
      alignItems : 'center',  
    },
});