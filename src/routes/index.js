import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Splash from '../pages/splash';
import WelcomeAuth from '../pages/welcome';
import Bingkai from '../pages/bingkai';
import Bingkai1 from '../pages/bingkai1';
import Bingkai2 from '../pages/bingkai2'; 
import Bingkai3 from '../pages/bingkai3';
import Bingkai4 from '../pages/bingkai4';
import Bingkai5 from '../pages/bingkai5';
import Bingkai6 from '../pages/bingkai6';
import Bingkai7 from '../pages/bingkai7';
import Bingkai8 from '../pages/bingkai8';
import Bingkai9 from '../pages/bingkai9';

const Stack =createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen
            name ="Splash"
            component={Splash}
            options ={{headerShown: false}}
            />
            <Stack.Screen
            name="Welcome"
            component={WelcomeAuth}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Doa Sebelum Makan"
            component={Bingkai}
            />
           <Stack.Screen
            name="Doa Sesudah Makan"
            component={Bingkai1}
            />
            <Stack.Screen
            name="Doa Sebelum Tidur"
            component={Bingkai2}
            />
            <Stack.Screen
            name="Doa Bangun Tidur"
            component={Bingkai3}
            />
            <Stack.Screen
            name="Doa Masuk Masjid"
            component={Bingkai4}
            />
             <Stack.Screen
            name="Doa Keluar Masjid"
            component={Bingkai5}
            />
             <Stack.Screen
            name="Doa Masuk Rumah"
            component={Bingkai6}
            />
             <Stack.Screen
            name="Doa Keluar Rumah"
            component={Bingkai7}
            />
            <Stack.Screen
            name="Doa Sebelum Belajar"
            component={Bingkai8}
            />
            <Stack.Screen
            name="Doa Sesudah Belajar"
            component={Bingkai9}
            />
            
        </Stack.Navigator>
    );
};

export default Route;